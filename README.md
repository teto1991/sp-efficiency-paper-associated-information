# Signal peptide efficiency: from high-throughput data to prediction and explanation #

### Stefano Grasso<sup>†</sup>, Valentina Dabene<sup>†</sup>, Margriet M.W.B. Hendriks, Priscilla Zwartjens, René Pellaux, Martin Held, Sven Panke, Jan Maarten van Dijl<sup>#</sup>, Andreas Meyer<sup>#</sup>, Tjeerd van Rij<sup>#</sup> ###
<sup>†,#</sup> These authors contributed equally

### Information assosciated with the paper ###

The present repository presents two folders:

* Supplementary_material: contains the 6 Supplementary Tables, the 9 Supplementary Figures, and the 3 Supplementary Files. This folder is also available as a zip package (Supplementary\_Material.zip) in this folder.
* Model_exploration: contains the Random Forest model itself and its associated data, together with a jupyter notebook to explore the SHAP model.

More informaion is contained in each folder. 

## Cite us 

## Contacs

You can contact us at: s [dot] grasso [at] umcg [dot] nl 

