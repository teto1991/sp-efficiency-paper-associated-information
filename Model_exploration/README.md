# INSTRUCTIONS #

If you want to explore the model in the associated paper, you need to open and run the Jupyter Notebook in this repository.
Have a look at the pdf and html files to have an idea of the expected output.



1. Clone the present repository in a folder of your choice 
	
	git clone https://teto1991@bitbucket.org/teto1991/sp-secretion-efficiency-jupyter-notebook.git



2. Create a suitable environemnt (Python > 3.6) and install relevant packages. 
   If you prefer you can use your local installation of Python. In such case install the same packages but use 'pip' (i.e. pip install)
		
		conda create -n SP-notebook python=3.6
		
		conda activate SP-notebook
		
		cd ‘directory-of-notebook’
		
		conda install jupyter
		
		conda install pandas
		
		conda install matplotlib
		
		conda install joblib
				
		conda install -c conda-forge shap
		
		jupyter notebook 



3. Navigate to the Jupyter notebook and run it. Feel free to play around.
   We suggest to refer to the docs of SHAP for a better understanding: https://shap.readthedocs.io/en/latest/



Feel free to contact us if you run into issues: s [dot] grasso [at] umcg [dot] nl
